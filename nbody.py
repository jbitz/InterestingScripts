#---------------------------------------------------
# nbody.py
# v.0.0.1
# by Jared Bitz, April 2016
#
# Simulates charges interacting, and draws their trajectories
# by approximating the relevant differential equations using Euler's method
#---------------------------------------------------

from graphics import *
from math import *

#Array accesors
COLOUMB_CONSTANT = 8.998e9
#COLOUMB_CONSTANT = 1
XPOS = 0
YPOS = 1
VELX = 2
VELY = 3
MASS = 4
CHARGE = 5

#Graphics constants
WINDOW_WIDTH = 800
WINDOW_HEIGHT = 800

COLORS = ["red", "purple", "green", "blue", "black", "orange", "brown", "grey"]
DISTANCE_THRESHOLD = 17 #Doesn't draw arrows if the line segments are shorter than this

def main():
    bodies = getBodies()
    numSeconds = float(input("How many seconds would you like to simulate? "))
    deltaT = numSteps = float(input("Choose a time step (things tend to break if it's < .1):"))

    win = setUpWindow()

    record = [[] for _ in range(int(numSeconds / deltaT))]
    #For window sizing later on
    minX = bodies[0][XPOS]
    maxX = bodies[0][XPOS]
    minY = bodies[0][YPOS]
    maxY = bodies[0][YPOS]

    #Perform simulation
    for i in range (0, int(numSeconds / deltaT)):
        newBodies = []
        for x in range(len(bodies)):
            newBodies.append(bodies[x][:])

        #calculate new position for each particle
        for cur in range(0, len(newBodies)):
            b = newBodies[cur][:]
            forceOnB = calculateNetForce(b, bodies)
            b[VELX] = b[VELX] + forceOnB[XPOS] / b[MASS] #vf = v0 + at
            b[VELY] = b[VELY] + forceOnB[YPOS] / b[MASS]
            b[XPOS] = b[XPOS] + deltaT * b[VELX] #xf = x0 + vt
            b[YPOS] = b[YPOS] + deltaT * b[VELY]
            if b[XPOS] < minX:
                minX = b[XPOS]
            if b[XPOS] > maxX:
                maxX = b[XPOS]
            if b[YPOS] < minY:
                minY = b[YPOS]
            if b[YPOS] > maxY:
                maxY = b[YPOS]
            newBodies[cur] = b[:]
        
        record[i] = newBodies[:]
        bodies = newBodies[:]

    maxX = maxX + (maxX - minX) * .1
    minX = minX - (maxX - minX) * .1
    maxY = maxY + (maxY - minY) * .1
    minY = minY - (maxY - minY) * .1

    drawTrajectories(record, win, minX, maxX, minY, maxY)

    print("Simulation complete!")
    input("Press enter to exit")
    win.close()

#user input
def getBodies():
    n = int(input("How many particles would you like to simulate? (8 or fewer)"))
    bodies = []
    print("Note: All quantities are in SI units (e.g. meters, Coulombs, etc.)")
    for i in range(0, n):
        print("=======Q" + str(i) + "=======")
        x = float(input("X position of particle " + str(i) + "? ")) 
        y = float(input("Y position of particle " + str(i) + "? ")) 
        vx = float(input("X velocity of particle " + str(i) + "? ")) 
        vy = float(input("Y velocity of particle " + str(i) + "? ")) 
        m = float(input("Mass of particle " + str(i) + "? ")) 
        c = float(input("Charge of particle " + str(i) + "? "))
        bodies.append([x, y, vx, vy, m, c])
    return bodies

#index = index of body for which to find force
def calculateNetForce(body, bodies):
    netForce = [0.0, 0.0]
    for i in range(0, len(bodies)):
        if bodies[i] == body:
            continue
        force = calculateForce(body, bodies[i])
        netForce[0] = netForce[0] + force[0]
        netForce[1] = netForce[1] + force[1]
    return netForce

#Calculates force of body 1 on body 2 with Coloumb's Law
def calculateForce(body1, body2):
    dx = body1[XPOS] - body2[XPOS]
    dy = body1[YPOS] - body2[YPOS]
    rSquared = dx * dx + dy * dy
    magnitude = (COLOUMB_CONSTANT * body1[CHARGE] * body2[CHARGE]) / rSquared
    forceX = magnitude * (dx / sqrt(rSquared))
    forceY = magnitude * (dy / sqrt(rSquared))
    return (forceX, forceY)

def setUpWindow():
    win = GraphWin("N-body particle simulation", WINDOW_WIDTH, WINDOW_HEIGHT)
    return win

#Plots trajectories given a particle history "record" and some window dimensions
def drawTrajectories(record, win, minX, maxX, minY, maxY):
    #fencepost
    for i in range(0, len(record[0])):
        p1Coord = toWindowCoordinates(record[0][i][XPOS], record[0][i][YPOS], minX, maxX, minY, maxY)
        p1 = Point(p1Coord[0], p1Coord[1])
        p1.setFill(COLORS[i])
        p1.draw(win)

        t = Text(p1, "Q" + str(i))
        t.setStyle("bold")
        t.draw(win)

    for t in range(1, len(record)):
        for i in range(0, len(record[t])):
            oldBodies = record[t - 1]
            newBodies = record[t]
            p1Coord = toWindowCoordinates(oldBodies[i][XPOS], oldBodies[i][YPOS], minX, maxX, minY, maxY)
            p2Coord = toWindowCoordinates(newBodies[i][XPOS], newBodies[i][YPOS], minX, maxX, minY, maxY)
            p1 = Point(p1Coord[0], p1Coord[1])
            p2 = Point(p2Coord[0], p2Coord[1])
            p2.setFill(COLORS[i])
            p2.draw(win)
            line = Line(Point(p1Coord[0], p1Coord[1]), Point(p2Coord[0], p2Coord[1]))
            line.setWidth(2)
            line.setFill(COLORS[i])
            if(shouldDrawArrow(p1Coord, p2Coord)): 
                line.setArrow("last")
            line.draw(win)

#Transfroms calculated coordinates into graphical window coordinates
def toWindowCoordinates(x, y, minX, maxX, minY, maxY):
    newX = int((x - minX) * (WINDOW_WIDTH / (maxX - minX)))
    newY = WINDOW_HEIGHT - int((y - minY) * (WINDOW_HEIGHT / (maxY - minY)))
    return (newX, newY)


def shouldDrawArrow(p1, p2):
    dx = p1[0] - p2[0]
    dy = p1[1] - p2[1]
    return sqrt(dx * dx + dy * dy) > DISTANCE_THRESHOLD

main()
