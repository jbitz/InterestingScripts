InterestingScripts
===========================

## nbody.py
Simulates the motion of charged particles in 2D space, based on Coulomb's law and Euler approximations. Written as an extra credit assignment for Stanford's PHYSICS 43

## diffeq.py
Runs various numerical approximations on a given first-order ODE. Includes Euler's method, an improved Euler's method, and the Runge-Kutta method. Written as a homework aide for Stanford's MATH 53
