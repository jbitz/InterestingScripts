#--------------------------------------
# diffeq.py
# v0.0.1 
# Jared Bitz, April 2016
# Uses various numerical methods to solve differential equations
# in the form y' = f(y, t)
#
# USAGE: Chanage the function "f" to be the right hand side
# of the differential equation you wish to approximate,
# then run the code and follow the prompts
#--------------------------------------

#Solution method identifiers
EULER = 1
IMPROVED_EULER = 2
RUNGE_KATTE = 3
 
#CHANGE THIS FUNCTION TO THE DESIRED DIFFERENTIAL EQUATION
#OF THE FORM y' = f(y, t)
def f(y, t):
    return 3 + t - y

def main():
    print("Welcome to the numerical differential equation solver!")
    y0 = float(input("Enter your starting y value, y0: "))
    t0 = float(input("Enter your starting t value, t0: "))
    tf = float(input("Enter your final t value, tf: "))
    h = float(input("Enter your time step value, h: "))

    print("Which approximation method would you like to use?")
    print("1) Euler's Method")
    print("2) Improved Euler's Method")
    print("3) Runge-Katte Method")
    choice = int(input("Your choice: "))
    

    #Build list of times to evaluate
    tList = []
    cur = t0
    while cur <= tf + tf * (h / 10): #I hate floating points (error bound < h)
        tList.append(cur)
        cur = cur + h

    #Initialize list of y coordinates to calculate
    #There are multiple lists here in case I come back and want to make it
    #Do them all at once
    eulerList = []
    eulerList.append(y0)
    improvedEulerList = []
    improvedEulerList.append(y0)
    rungeKatteList = []
    rungeKatteList.append(y0) 
    yList = []
    if choice == EULER:
        euler(tList, eulerList, h)
        yList = eulerList
    if choice == IMPROVED_EULER:
        improvedEuler(tList, improvedEulerList, h)
        yList = improvedEulerList
    if choice == RUNGE_KATTE:
        rungeKatte(tList, rungeKatteList, h)
        yList = rungeKatteList

    print()
    print("Results:")
    print("t\ty")
    print("-----------------")
    for i in range(len(yList)):
        print(str(tList[i])[:5] + ":\t" + str(yList[i]))
   
#Euler's method of approximation - the most basic
def euler(tList, yList, h):
    for i in range(1, len(tList)):
        yList.append( yList[i - 1] + f( yList[i - 1], tList[i - 1] ) * h )

#Improved Euler's method - based on the trapezoid rule
def improvedEuler(tList, yList, h):
    for i in range(1, len(tList)):
        yPrev = yList[i - 1]
        tPrev = tList[i - 1]
        yPrime = yPrev + f( yPrev, tPrev ) * h #Euler approx
        yList.append( yPrev + (h / 2.0) * ( f(yPrev, tPrev) + f(yPrime, tList[i] )) )

#Runge-Katte method - based on Simpson's rule
def rungeKatte(tList, yList, h):
    for i in range(1, len(tList)):
        yPrev = yList[i - 1]
        tPrev = tList[i - 1]
        k1 = f(yPrev, tPrev)
        k2 = f(yPrev + .5 * h * k1, tPrev + .5 * h)
        k3 = f(yPrev + .5 * h * k2, tPrev + .5 * h)
        k4 = f(yPrev + h * k3, tList[i])
        yList.append( yPrev + (h / 6.0) * (k1 + 2.0 * k2 + 2.0 * k3 + k4) )

main()
